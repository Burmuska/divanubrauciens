<?php

namespace App\Http\Controllers;

use App\Post;

class HomeController extends Controller
{
    public function index(string $lang = null)
    {
        if ( !$lang ) {
            return redirect('/lv');
        }

        $posts = Post::orderBy('published_at', 'desc')->limit(3)->get();
        return view('pages.home', [
            'posts' => $posts,
        ]);
    }
}
