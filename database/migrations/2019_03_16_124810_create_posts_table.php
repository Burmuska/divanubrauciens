<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 30)->commet('about, news, gallery');
            $table->string('title', 255);
            $table->string('slags', 150)->index()->comment('userfriendly url name');
            $table->text('body_html')->comment('post content');
            $table->string('status', 30)->default('draft')->comment('active, draft, deleted');
            $table->string('lang', 5)->comment('lv, ru, en');
            $table->dateTime('published_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
