<?php

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'type' => 'news',
        'title' => $faker->sentence(),
        'slags' => implode('-', $faker->words()),
        'body_html' => $faker->realText(),
        'status' => 'active',
        'lang' => 'lv',
        'published_at' => now(),
    ];
});
