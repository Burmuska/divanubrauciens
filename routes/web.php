<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{lang}/about', 'PostController@about');
Route::get('/{lang}/posts', 'PostController@index');
Route::get('/{lang}/posts/{post}', 'PostController@show');
Route::get('/{lang}/contacts', 'ContactController@index');

Route::get('/{lang?}', 'HomeController@index');    // Home/ first page
