@extends('layouts.app')

@section('title', 'Jaunumi')

{{-- @component('navigation')
    <strong>Whoops!</strong> Something went wrong!
@endcomponent --}}

@section('content')

   @foreach ($posts as $post)

      <div class="row">

         <h2>{{$post->title}}</h2>
         <p>{!! $post->body_html !!}</p>

      </div>

   @endforeach

@endsection