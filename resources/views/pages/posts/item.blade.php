@extends('layouts.app')

@section('title', 'Ziņa')

@section('content')

    <h2>{{$post->title}}</h2>
    <p>{!! $post->body_html !!}</p>

@endsection
