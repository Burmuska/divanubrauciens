@extends('layouts.app')

@section('content')

   @foreach ($posts as $post)
        <h2>{{$post->title}}</h2>
        <p>{!! $post->body_html !!}</p>
   @endforeach

@endsection

<!-- links uz pārējiem Jaunumiem -->
<!-- tuvākais datums -->
<!-- sponsori -->