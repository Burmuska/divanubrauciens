@extends('layouts.app')

@section('title', 'Par mums')

@section('content')

    <h2>{{$post->title}}</h2>
    <p>{!! $post->body_html !!}</p>

@endsection