@extends('layouts.app')

@section('title', 'Kontakti')


@section('content')

<div class="row">
   @foreach ($persons as $person)
        <div class="col-lg-12">
            
            <b>{{$person->name}}</b>
             </br>
            <p>{{ $person->email }} {{ $person->phone }}</p>

        </div>
    @endforeach
</div>

@endsection