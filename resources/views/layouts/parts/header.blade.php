<div class="container text-center">
<div class="jumbotron">
            <h1><a href="/">Dīvānu brauciens</a></h1>
            <p>Attēls vai kas līdzīgs</p>
        </div>
    </div>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        {{-- <a class="navbar-brand" href="#">Navbar</a> --}}
        {{-- <ul class="nav">
            <li><a href="/">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/projects">Projects</a></li>
            <li><a href="/contact">Contact</a></li>
        </ul> --}}
        <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="/lv/about">Par mums <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    {{-- <a class="nav-link" href="/{{app()->getLocale())}}/posts">Jaunumi</a> --}}
                    <a class="nav-link" href="/lv/posts">Jaunumi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lv/gallery">Galerija</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lv/sponsors">Atbalstītāji</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lv/contacts">Kontakti</a>
                </li>
            </ul>        
        </div>
    </nav>
</div>
