<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.parts.head')
    </head>

    <body>

        <div class="container">

            <header class="row">
                @include('layouts.parts.header')
            </header>
        
            <div id="main" class="row">
        
                    @yield('content')
        
            </div>
        
            <footer class="row">
                @include('layouts.parts.footer')
            </footer>
        
        </div>

        @include('layouts.parts.footer-scripts')
    </body>
</html>
